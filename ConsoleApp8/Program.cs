using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Grapevine.Server;
using Grapevine.Server.Attributes;
using Grapevine;
using Grapevine.Interfaces.Server;
using Grapevine.Shared;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json.Linq;

namespace iPMS_RestRelayServ
{
    class Program
    {
        public static List<Event> EventsList = new List<Event>();

        static void Main(string[] args)
        {

            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

            var server = new RestServer();

            //server.UseHttps = true;

            server.Port = "9095";
            server.Host = "*";

            server.Start();



            Console.WriteLine("KAS Lock-S Rest plugin started");

            while (server.IsListening)
            {
                Thread.Sleep(300);
            }

            // This little tidbit will keep your console open after the server
            // stops until you explicitly close it by pressing the enter key

            Console.ReadLine();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 



        }

        private static async Task<Boolean> callbackWatch(string url)
        {
            int result = 0;

            while (result != 1)
            {
                result = await getStatus(url);

                await Task.Delay(3000);
            }

            return true;
        }

        private static async Task<string> getToken()
        {
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://cloud.kas.com.au/api/accesstoken?token=6688");
            //request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            //using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
            //using (Stream stream = response.GetResponseStream())
            //using (StreamReader reader = new StreamReader(stream))
            //{
            //string test = await reader.ReadToEndAsync();
            //Console.WriteLine(test);

            //return test;
            //}

            return "60ce9e8eb82243aa49c7ea7ebd8d47935638282bca830254";
        }

       

        private static async Task<int> getStatus(string url)
        {
            string token = await getToken();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers.Add("Authorization", "Bearer " + token);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                string test = await reader.ReadToEndAsync();
                Console.WriteLine(test);
                
                var getStatID = JObject.Parse(test)["info"]["int_status"];

                //var getStatIDFinal = getStatID["info"]; 
                int getStatIDInt = Int32.Parse(getStatID.ToString()); 

                
                Console.WriteLine(getStatID);

                return getStatIDInt;


            }
        }

        private void getSocketReplay2(){
            using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
            {
                client.BaseAddress = new Uri("https://api.stackexchange.com/2.2/");
                HttpResponseMessage response = client.GetAsync("answers?order=desc&sort=activity&site=stackoverflow").Result;
                response.EnsureSuccessStatusCode();
                string result = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine("Result: " + result);
            }
        }

        [RestResource]
        public class TestResource
        {


            [RestRoute(HttpMethod = Grapevine.Shared.HttpMethod.GET, PathInfo = "/send")]
            public IHttpContext SendCommand(IHttpContext context)
            {
                var word = context.Request.QueryString["command"] ?? "command not specified?";

                var username = context.Request.QueryString["username"] ?? "username not specified?";

                var password = context.Request.QueryString["password"] ?? "password not specified?";

                var ipAddress = context.Request.RemoteEndPoint.Address.ToString();

                if (string.Compare(ipAddress,"::1") == 0)
                {
                    ipAddress = "localhost";
                }

                var userName = context.Request.Headers["Authorization"];

                //string encodedUsernamePassword = userName.Substring("Basic ".Length).Trim();

                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                //string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                
                Console.WriteLine("received command: " +  word + DateTime.Now + ipAddress + "username: " + username + "passname: " + password);

                EventsList.Add(new Event(DateTime.Now,"  --> Command Received: (" + word + ")   from ip: " + ipAddress));

              
                try
                {
                    Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    socket.Connect("localhost", 10003);

                    //byte[] rb = new byte[256];

                    byte[] bytesSend;

                    byte[] bytesReceived = new byte[1024];

                    word = "9800O|R0101|UAdmin";
                    //word = "0101B|H+61406174727";
                    //word = "0101I|R0101|NDuck|D201908261200|O201908302100|H+61406174727#12345678@qq.com";

                    bytesSend = Encoding.UTF8.GetBytes(String.Format(word));

                    List<byte> test = bytesSend.ToList();

                    test.Insert(0, 0x02);
                    test.Add(0x03);

                    bytesSend = test.ToArray();

                    socket.Send(bytesSend, bytesSend.Length, 0);

                    EventsList.Add(new Event(DateTime.Now, "  <-- Command Send: (" + word + ")   to ip: " + "localHost:1234"));

                    int byteRecv = socket.Receive(bytesReceived);

                  
                    string messageReceivedText = Encoding.ASCII.GetString(bytesReceived, 0, byteRecv);

                    Console.WriteLine(messageReceivedText);

                    string[] extractID = messageReceivedText.Split('"');

                    string statusID = null;

                    foreach (var item in extractID)
                    {
                        if (item.Length > 6)
                        {
                            statusID = item;
                        }
                    }

                    Console.Write(statusID);

                    //var received = startCallaback();

                   

                   // async Task<bool> startCallaback()
                    //{
                        //bool callbackStatus = false;

                        callbackWatch("https://lock.ufunnetwork.com/ilocks/api/apps/v1/servers/instructions/" + statusID + "/status");

                       // callbackStatus = receivedStatus;

                       // return callbackStatus;
                    //}

                        //Console.Write(receivedStatus);

                    context.Response.SendResponse("success");

                    EventsList.Add(new Event(DateTime.Now, "  <-- Received Ack: (" + messageReceivedText + ")   from ip: " + "localHost:1234"));

                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                }
                catch (Exception e)
                {
                    EventsList.Add(new Event(DateTime.Now, "  <-- Command Send: (Failed to connect to Milestone Event Server) ip: " + "localHost:1234"));
                }

               

                return context;
            }

            [RestRoute(HttpMethod = Grapevine.Shared.HttpMethod.GET, PathInfo = "/events")]
            public IHttpContext SendEvents(IHttpContext context)
            {
                //var word = context.Request.QueryString["command"] ?? "what?";

                //var userName = context.Request.Headers["Authorization"];

                //string encodedUsernamePassword = userName.Substring("Basic ".Length).Trim();

                //Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                //string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
                var response = JsonConvert.SerializeObject(EventsList);

                context.Response.SendResponse(response);
                //Console.WriteLine("received command: " + usernamePassword);

                //EventsList.Add(new Event(DateTime.Now, word));

                //Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //socket.Connect("localhost", 1234);

                //byte[] rb = new byte[256];

                //byte[] bytes;

                //bytes = Encoding.UTF8.GetBytes(String.Format(word));

                //socket.Send(bytes, bytes.Length, 0);

                //socket.Close();

                return context;
            }

            [RestRoute]
            public IHttpContext Status(IHttpContext context)
            {
                string events = "";

                foreach(var item in EventsList){
                    events = events +  "<p>" + item.now + " " + item.word + "</p>";
                }

                context.Response.SendResponse("<html><head><script>var myVar = setInterval(myTimer, 1000); function myTimer() {var d = new Date(); document.getElementById(\"demo\").innerHTML = d.toLocaleTimeString();}" +
                    "</script></head><body><h1>Server logs</h1><p id=\"demo\"></p>" + events + "</body></html>");
                return context;
            }

        }

        public class Event
        {
            public DateTime now;
            public string word;

            public Event(DateTime now, string word)
            {
                this.now = now;
                this.word = word;
            }


          
        }
    }

    

}

